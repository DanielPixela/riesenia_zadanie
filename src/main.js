// Import all styles
import '@/assets/sass/app.scss';

// Import Vue components
import TheHeader from "@/components/layout/TheHeader";
import TheFooter from "@/components/layout/TheFooter";
import BaseButton from "@/components/UI/BaseButton";
import BaseList from "@/components/UI/BaseList";

// Import router
import router from "../router";

// Import Event Bus
import mitt from 'mitt'

// Import VueSweetalert 2
import VueSweetalert2 from 'vue-sweetalert2';


// Create Vue project
import { createApp, nextTick } from 'vue';
import App from './App.vue';

const app = createApp(App);
const emitter = mitt();


router.beforeEach((to) => {
    nextTick(()=> {
        document.title = to.meta.title;
    })
})
app.component('the-header', TheHeader);
app.component('the-footer', TheFooter);
app.component('base-button', BaseButton);
app.component('base-list', BaseList);

app.use(router);
app.use(VueSweetalert2);

app.config.globalProperties.emitter = emitter;
app.mount('#app');
