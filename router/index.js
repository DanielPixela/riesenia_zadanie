// Import vue-router
import {createRouter, createWebHistory} from 'vue-router';

// Import pages
import PageHome from "@/components/pages/PageHome";

// Default title
const default_title = '- Riešenia.com';

// Define routes
const routes = [
    {
        name: 'Home',
        path: '/',
        component: PageHome,
        meta: {
            title: `Homepage ${default_title}`
        }
    }
]

// Create router
const router = createRouter({
    history: createWebHistory(),
    routes
})


export default router;